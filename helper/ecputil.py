import sys
import argparse
import jsonpath
import yaml
import time
from ecp import ECP

class ECPUtil(ECP):

    def checkStatus(self, deploymentid, key='status'):
        status = 'STARTING'

        r = self.make_request('get', 'status', deploymentid)
        print(r.text)

       # watch out for the good old timeout error
        if r.status_code == 401:
            print('Got 401 unauthorized, please run \'ecp login\' to log in.')
            return status

        r_json = r.json()
        status = r_json[key]

        return status

#Not in use. replaced by _replaceYamlValue
    def __replaceYamlValue(self, yfile, ypath, yvalue, target, separator='.'):
        with open(yfile, 'r') as fp:
            yaml_obj = yaml.load(fp)
        last_dict = _yaml_obj = yaml_obj

        key_list = ypath.split(separator)
        for key in key_list:
            last_dict = _yaml_obj
            try:
                _yaml_obj = _yaml_obj[key]
            except NotADirectoryError:
                return _yaml_obj
            except KeyError:
                return _yaml_obj
        last_dict[key] = yvalue

        with open(target, 'w') as fp:
            yaml.dump(yaml_obj, fp, indent=2, default_flow_style=False)

        return _yaml_obj

    def _replaceYamlValue(self, yfile, ypath, yvalue, target):
        i = ypath.rindex('.')
        lead_path = ypath[:i]
        last_key = ypath[i+1:]

        with open(yfile, 'r') as fp:
            yaml_obj = yaml.load(fp)

        # jsonpath returns a list of one dictionary instead of a dictionary
        result = jsonpath.jsonpath(yaml_obj, lead_path).pop(0)
        old_value = result[last_key]
        result[last_key] = yvalue

        with open(target, 'w') as fp:
            yaml.dump(yaml_obj, fp, indent=2, default_flow_style=False)

        return old_value

    def _getYamlValue(self, yfile, ypath):
        with open(yfile, 'r') as fp:
            yaml_obj = yaml.load(fp)
        result = jsonpath.jsonpath(yaml_obj, ypath).pop(0)
        return result

def main(argv):
    parser = argparse.ArgumentParser(description='Utility for ECP automation')

    verbs = ['getAccessIp', 'getAppName', 'getDeploymentId', 'getPrivateIp', 'replaceYamlValue', 'waitForDeployment']
    parser.add_argument('verb', help='Action to be performed, expecting one of: '.join(verbs))
    parser.add_argument('data' , help='Valid YAML file to be parsed')
    parser.add_argument('--value', help='New value to be stored into YAML')
    parser.add_argument('--target', help='Target file name after parsing')
    args = parser.parse_args()

    e = ECPUtil()

    if not args.verb in verbs:
        print("Unknown verb " + args.verb + ", expecting one of: " + ', '.join(verbs))
        return

    if args.verb == 'getAccessIp':
        ip = ''
        key='$.accessIp'
        ip = e._getYamlValue(yfile=args.data, ypath=key)
        print(ip)

    if args.verb == 'getAppName':
        app_name = ''
        key='$.applicationName'
        app_name = e._getYamlValue(yfile=args.data, ypath=key)
        print(app_name)

    if args.verb == 'getDeploymentId':
        id = ''
        key='$.reference'
        id = e._getYamlValue(yfile=args.data, ypath=key)
        print(id)

    if args.verb == 'getPrivateIp':
        ip = ''
        key = '$.generatedOutputs[?(@.outputName=="private_ip")].generatedValue'
        ip = e._getYamlValue(yfile=args.data, ypath=key)
        print(ip)

    if args.verb == 'replaceYamlValue':
        old_value = ''
        key = '$.spec.nfs.server'

        if args.value is None or args.target is None:
            required = ['value', 'target']
            print("Missing required parameter(s): ".join(required))
            return

        old_value = e._replaceYamlValue(yfile=args.data, ypath=key, yvalue=args.value, target=args.target)
        print(old_value)

    if args.verb == 'waitForDeployment':
        i = 0
        status = e.checkStatus(deploymentid=args.data)
        print('Status: (' + str(i) + 's) ' + status + '...')

        while not (status == 'RUNNING' or status == 'STARTING_FAILED' or status == 'DESTROYED'):
            time.sleep(10)
            i = i + 10
            status = e.checkStatus(deploymentid=args.data)
            print('Status: (' + str(i) + 's) ' + status + '...')

if __name__ == '__main__':
    main(sys.argv)
